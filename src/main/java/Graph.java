
import java.util.ArrayDeque;
import java.util.ArrayList;

public class Graph {

    public static void main(String[] args) {


        ArrayList<String> strings = new ArrayList<String>();
        Graph graph = new Graph();
        graph.method(new boolean[][]{{false, true, true, false, false},
                {true, false, true, false, false},
                {true, true, false, true, true},
                {false, false, true, false, true},
                {false, false, true, true, false}}, 3);
    }


    void method(boolean[][] matrix, int index) {
        ArrayDeque<Integer> vertexes = new ArrayDeque<Integer>();
        ArrayList<Integer> result = new ArrayList<Integer>();
        vertexes.addFirst(index);
        result.add(index);
        while (!vertexes.isEmpty()) {
            for (int i = 0; i < matrix[index-1].length; i++) {
                if (matrix[index-1][i]) {
                    if (check(result, i + 1)) {
                        vertexes.addLast(i + 1);
                        result.add(i + 1);
                    }
                }

            }
            vertexes.pollFirst();
            try {
                index = vertexes.peekFirst();
            } catch (NullPointerException e) {
                System.out.println(result);
            }

        }
    }

    boolean check(ArrayList<Integer> result, Integer check) {

        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).equals(check)) {
                return false;
            }
        }
        return true;
    }
}
